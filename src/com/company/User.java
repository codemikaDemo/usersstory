package com.company;

public class User {
    String name;
    String surname;
    User(String name){
        this.name = name;
    }
    User(String name, String surname){
        this.name = name;
        this.surname = surname;
    }
    public void print(){
        System.out.println(this.surname + " " + this.name);
    }
}
